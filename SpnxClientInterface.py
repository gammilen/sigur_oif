from Command import *
from Session import *

class SpnxClientInterface():
	
	def __init__(self, connection):
		commands_obj = [ 
				LoginCommand(connection, 'login'),
				GetObjectInfoCommand(connection, 'get object info'),
				SubscribeCommand(connection, 'subscribe'),
				UnsubscribeCommand(connection, 'unsubscribe'),
				GetApListCommand(connection, 'get ap list'),
				GetApInfoCommand(connection, 'get ap info'),
				GetHistoryCommand(connection, 'get history'),
				GetZoneInfoCommand(connection, 'get zone info'),
				GetLocationCommand(connection, 'get location'),
				SetApModeCommand(connection, 'set ap mode'),
				AllowPassCommand(connection, 'allow pass'),
				]
		# словарь имя команды : команда
		self.commands = {command.name:command for command in commands_obj}
		self.is_active = True # после инициализации интерфейс активируется


	def show_commands(self):
		for command in self.commands.keys():
			print(command)


	def process_command(self):
		command = input('Введите команду: ')
		try:
			if command == 'show':
				self.show_commands()
			elif command == 'help':
				self.help_command()
			elif command == 'quit':
				self.is_active = False
			elif command in self.commands.keys():
				self.commands[command].execute()
		except:
			record = 'Неудачное выполнение команды {}'
			SpnxClientInterface.add_log_record(record.format(command), 'info')
			raise
		else:
			record = 'Удачное выполнение команды {}'
			SpnxClientInterface.add_log_record(record.format(command), 'info')


	def help_command(self):
		command = input('Введите команду для вывода справки: ')
		if command in self.commands.keys():
			print(self.commands[command].info)


	@staticmethod
	def add_log_record(record, level):
		try:
			record = '[ИНТЕРФЕЙС] ' + record
			func = getattr(Logger('connection'), level)(record)
		except:
			print('Ошибка логирования')