#from exceptions import *
import threading


#errors = {
#	'4': NotLoggedInError()
#}
bufsize = 1024



def login(socket, interface, login, password):
	_send(socket, 'LOGIN {interface} {login} {password}'.format(interface=interface, login=login, password=password))
	response = _get_answer(socket)
	return response

def get_object_info(socket, object_id):
	if object_id == 'ALL': # нужно в любом регистре
		_send(socket, 'GETOBJECTINFO ALL')
	else:
		_send(socket, 'GETOBJECTINFO OBJECTID {object_id}'.format(object_id=object_id))
	response = _get_answer(socket)
	return response

def get_zone_info(socket):
	_send(socket, 'GETZONEINFO')
	response = _get_answer(socket)
	return response

def subscribe(socket, subscription_type):
	_send(socket, 'SUBSCRIBE {}'.format(subscription_type))
	response = _get_answer(socket)
	return response

def unsubscribe(socket):
	_send(socket, 'UNSUBSCRIBE')
	response = _get_answer(socket)
	return response

def get_event(subscribe_socket):
	response = _get_answer(subscribe_socket)
	return response

def get_ap_list(socket):
	_send(socket, 'GETAPLIST')
	response = _get_answer(socket)
	return response

def get_ap_info(socket, ap_id):
	_send(socket, 'GETAPINFO {}'.format(ap_id))
	response = _get_answer(socket)
	return response

def get_history(socket, from_time, till_time):
	_send(socket, 'GETHISTORY FROM "{}" TILL "{}"'.format(from_time, till_time))
	response = _get_answer(socket)
	return response

def get_location(socket, version, object_id):
	if version == '1':
		version = '' # для первой версии отправляется 'GETLOCATION id'
	_send(socket, 'GETLOCATION{} {}'.format(version, object_id))
	response = _get_answer(socket)
	return response

def set_ap_mode(socket, mode, ap_list):
	_send(socket, 'SETAPMODE {} {}'.format(mode, ap_list))
	response = _get_answer(socket)
	return response

def allow_pass(socket, ap_id, obj, direction):
	_send(socket, 'ALLOWPASS {} {} {}'.format(ap_id, obj, direction))
	response = _get_answer(socket)
	return response

# def sync_db(socket):
# 	pass

# def delegation_start(socket):
# 	_send(socket, 'DELEGATION START')
# 	response = _get_answer(socket)
# 	return response

# def delegation_stop(socket):
# 	_send(socket, 'DELEGATION STOP')
# 	response = _get_answer(socket)
# 	return response

# def delegation_request(socket):
# 	pass

def _send(socket, msg=''):
	socket.send(str.encode(msg+'\r\n'))

def _get_answer(socket):
	response = ''
	while True:
		tmp = bytes.decode(socket.recv(bufsize))
		if not tmp: raise BrokenPipeError #не совсем уверена
		response += tmp
		if str.find(tmp, "\r\n"): 
			break
	#_process_error(response)
	return response

def _process_error(response):
	pass
	#if response.startswith('ERROR'):
	#	raise errors[response.split(' ')[1]]

