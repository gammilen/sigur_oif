import sys
import time as t
import socket
from Session import *
from SpnxClientInterface import SpnxClientInterface
from Command import *

#logger = Logger()

class Connection:

	__instance = None
	
	@staticmethod
	def getInstance():
		if Connection.__instance == None:
			try:
				Connection()
			except ConnectionRefusedError:
				record = 'Не удается соединиться с сервером'
				print(record)
				Connection.add_log_record(record, 'error')
				sys.exit(1)
			except socket.error:
				record = "Не удалось создать сокет"
				print(record)
				Connection.add_log_record(record, 'error')
			except (socket.gaierror, socket.herror):
				record = 'Проблема данных для подключения'
				print(record)
				Connection.add_log_record(record, 'error')
				sys.exit(1)
		return Connection.__instance
	

	def __init__(self):
		# достаем из конфига значения ip и port
		self.ip = ConfigParser().get('SERVER','ServerIp') 
		self.port = int(ConfigParser().get('SERVER','ServerPort'))
		#self.ip = ip
		#self.port = port
		self.client_interface = None
		self.background_sock = None
		self.socket = Connection.init_socket(self.ip, self.port)
		Connection.__instance = self
		

	@staticmethod
	def init_socket(ip, port):
		time = 1
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		while time <= 10: # пытаемся подключиться 10 раз
			try:
				s.connect((ip, port))
			except ConnectionRefusedError: 
				time += 1
				t.sleep(1) # ждем, чтобы подключиться заново
			else:
				record = 'Соединение успешно установлено c {} попытки'.format(time)
				Connection.add_log_record(record, 'info')
				return s
		raise ConnectionRefusedError #не удалось подключиться за 10 раз


	@property
	def background_socket(self):
		if not self.background_sock:
			self.background_sock = self.init_socket(self.ip, self.port)
		return self.background_sock
	

	@property
	def main_socket(self):
		return self.socket


	# def close_background_socket(self):
	# 	if self.background_sock:
	# 		self.background_socket.close()
	# 		self.background_sock = None

	
	def start_exchanging(self):
		self.initClientInterface()
		while True:
			try:
				self.client_interface.process_command()
				if not self.client_interface.is_active: # если закончили работу с интерфейсом, выходим из цикла
					break
			except (BrokenPipeError, ConnectionResetError):
				record = "Соединение разорвано"
				print(record)
				Connection.add_log_record(record, 'error')
				sys.exit(1)
			except KeyboardInterrupt:
				record = "Соединение разорвано пользователем"
				print(record)
				Connection.add_log_record(record, 'info')
				sys.exit(1)
		Connection.add_log_record('Сеанс завершен', 'info')

	def end_exchanging(self):
		self.socket.close()
		if self.background_sock:
			self.background_sock.close()
				
	@staticmethod
	def add_log_record(record, level):
		try:
			record = '[{ip},{port}] ' + record
			record = record.format(ip=ConfigParser().get('SERVER','ServerIp'), 
				port=ConfigParser().get('SERVER','ServerPort'))
			getattr(Logger(), level)(record)
		except:
			print('Ошибка логирования')
		
	def initClientInterface(self):
		self.client_interface = SpnxClientInterface(self)