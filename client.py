#!/usr/bin/python3
from Connection import Connection


def main():
	connection = Connection.getInstance()
	connection.start_exchanging()
	connection.end_exchanging()

	
if __name__ == '__main__':
	main()
