from abc import ABC, abstractmethod
import Connection
import spnx_commands as spnx
import threading


class Command(ABC):

	@abstractmethod
	def __init__(self, connection: Connection, name):
		self.name = name
		self.connection = connection
		self.parameters = None

	@abstractmethod
	def execute(self):
		pass

	@property
	@abstractmethod
	def socket(self):
		pass

	@staticmethod
	def add_log_record(record, level): #TODO удалить?
		record = '[КОМАНДА] ' + record
		self.connection.add_log_record(record, level)

	def readParams(self):
		for parameter in self.parameters:
			print('Введите значение для {}:'.format(parameter.name))
			if not parameter.message == '':
				print('({})'.format(parameter.message)) # выводим примечание
			data = input()
			parameter.value = data

	def cancel(self):
		print('Ваша команда {} отменена'.format(self.name))


class Parameter():

	def __init__(self, name, value='', message=''):
		self.name = name
		self.value = value
		self.message = message
	

class LoginCommand(Command):

	def __init__(self, connection: Connection, name):
		super(LoginCommand, self).__init__(connection, name)
		self.int_ver = Parameter('interface version')
		self.login = Parameter('login')
		self.password = Parameter('password')
		self.sock = None
		self.parameters = [self.int_ver, self.login, self.password]
		self.info = 'Подключение пользователя к серверу'

	@property
	def socket(self):
		if not self.sock: 
			self.sock = self.connection.main_socket # по умолчанию главный сокет подключения
		return self.sock

	@socket.setter
	def socket(self, socket): # есть возможность поставить другой сокет для логина
		self.sock = socket

	def execute(self):
		self.readParams()
		response = spnx.login(self.socket, self.int_ver.value, 
			self.login.value, self.password.value)
		print(response)


# Возвращает информацию о сотрудниках/гостях/машинах: id, name/number, position/model, tabnumber		
class GetObjectInfoCommand(Command):

	def __init__(self, connection: Connection, name):
		super(GetObjectInfoCommand, self).__init__(connection, name)
		self.object_id = Parameter('object id', message='id или all')
		self.parameters = [self.object_id]
		self.info = 'Информация об объектах доступа'

	@property
	def socket(self):
		return self.connection.main_socket 

	def execute(self):
		self.readParams()
		response = spnx.get_object_info(self.socket, self.object_id.value)
		print(response)
		

# Возвращает только id точек доступа или EMPTY
class GetApListCommand(Command):

	def __init__(self, connection: Connection, name):
		super(GetApListCommand, self).__init__(connection, name)
		self.info = 'Существующие точки доступа'

	@property
	def socket(self):
		return self.connection.main_socket

	def execute(self):
		response = spnx.get_ap_list(self.socket)
		print(response)


# Возвращает ID точки доступа, ее имя и состояние, а также ID зоны со стороны входа и со стороны выхода
class GetApInfoCommand(Command):

	def __init__(self, connection: Connection, name):
		super(GetApInfoCommand, self).__init__(connection, name)
		self.ap_id = Parameter('ap id') # id точки доступа
		self.parameters = [self.ap_id]
		self.info = 'Информация о точке доступа'

	@property
	def socket(self):
		return self.connection.main_socket

	def execute(self):
		self.readParams()
		response = spnx.get_ap_info(self.socket, self.ap_id.value)
		print(response)


# Возвращает список событий прохода/запрета прохода с их описанием
class GetHistoryCommand(Command):

	def __init__(self, connection: Connection, name):
		super(GetHistoryCommand, self).__init__(connection, name)
		self.from_date_time = Parameter('from', 
			message='время в формате ГГГГ-ММ-ДД ЧЧ:ММ:СС')
		self.till_date_time = Parameter('till', 
			message='время в формате ГГГГ-ММ-ДД ЧЧ:ММ:СС')
		self.parameters = [self.from_date_time, self.till_date_time]
		self.info = 'Информация о событиях в системе за определенный период'

	@property
	def socket(self):
		return self.connection.main_socket

	def execute(self):
		self.readParams()
		response = spnx.get_history(self.socket, 
			self.from_date_time.value, self.till_date_time.value)
		print(response)


class SubscribeCommand(Command): 
	sub_file = 'subscribe.log'

	def __init__(self, connection: Connection, name):
		super(SubscribeCommand, self).__init__(connection, name)
		self.login_command = LoginCommand(connection, name) # используется другой сокет, поэтому необходимо войти
		self.subscription_type = Parameter('subscription type', 
			message='NOFILTER/RTONLY/CE/можно оставить пустым')
		self.info = 'Подписка на уведомления о событиях \
		системы в режиме реального времени'
		self.parameters = [self.subscription_type]

	@property
	def socket(self):
		return self.connection.background_socket
	
	def execute(self):
		print('Для подписки войдите заново')
		self.login_command.socket = self.socket
		self.login_command.execute()
		
		self.readParams()
		response = spnx.subscribe(self.socket, self.subscription_type.value)
		print(response)
		if threading.active_count() == 1: # если нет дополнительного потока для прослушивания событий, создаем новый
			t = threading.Thread(target=self.write_events, daemon=True)
			t.start()
		
	def write_events(self):
		with open(self.sub_file, 'a') as file:
			file.write(spnx.get_event(self.socket))
			

class UnsubscribeCommand(Command):

	def __init__(self, connection: Connection, name):
		super(UnsubscribeCommand, self).__init__(connection, name)
		self.info = 'Отписаться от уведомлений о событиях системы'

	@property
	def socket(self):
		return self.connection.background_socket

	# def close_socket(self):
	# 	self.connection.close_background_socket()

	def execute(self):
		if not threading.active_count() == 1:
			for thread in threading.enumerate():
				if not thread == threading.current_thread():
					thread.join(0.1) # прерываем потоки кроме текущего, т.о. освобождаем сокет
		response = spnx.unsubscribe(self.socket)
		print(response)


# Возвращает ID и NAME зон
class GetZoneInfoCommand(Command):

	def __init__(self, connection: Connection, name):
		super(GetZoneInfoCommand, self).__init__(connection, name)
		self.info = 'Информация об имеющихся зонах и их идентификаторов'

	@property
	def socket(self):
		return self.connection.main_socket

	def execute(self):
		response = spnx.get_zone_info(self.socket)
		print(response)


# Возвращает id зоны и время последнего обновления информации
class GetLocationCommand(Command):

	def __init__(self, connection: Connection, name):
		super(GetLocationCommand, self).__init__(connection, name)
		self.version = Parameter('version', message='1 или 2')
		self.object_id = Parameter('object id')
		self.parameters = [self.version, self.object_id]
		self.info = 'Получение текущего метоположения объекта доступа'	

	@property
	def socket(self):
		return self.connection.main_socket	

	def execute(self):
		self.readParams()
		response = spnx.get_location(self.socket, 
			elf.version.value, self.object_id.value)
		print(response)


class SetApModeCommand(Command):

	def __init__(self, connection: Connection, name):
		super(SetApModeCommand, self).__init__(connection, name)
		self.mode = Parameter('mode', message='NORMAL/LOCKED/UNLOCKED')
		self.ap_list = Parameter('ap list', message='id через пробелы/ALL')
		self.parameters = [self.mode, self.ap_list]
		self.info = 'Установка режима блокировки точек доступа'

	@property
	def socket(self):
		return self.connection.main_socket	

	def execute(self):
		self.readParams()
		response = spnx.set_ap_mode(self.socket, 
			self.mode.value, self.ap_list.value)
		print(response) 	


class AllowPassCommand(Command):

	def __init__(self, connection: Connection, name):
		super(AllowPassCommand, self).__init__(connection, name)
		self.ap_id = Parameter('ap id') # id точки доступа
		self.obj = Parameter('obj', message='ANONYMOUS или obj id') # id объекта доступа
		self.direction = Parameter('direction', message='IN/OUT/UNKNOWN')
		self.parameters = [self.ap_id, self.obj, self.direction]
		self.info = 'Санкционирование однократного прохода через точку доступа'

	@property
	def socket(self):
		return self.connection.main_socket	

	def execute(self):
		self.readParams()
		response = spnx.allow_pass(self.socket, self.ap_id.value, 
			self.obj.value, self.direction.value)
		print(response) 


# class DelegationStartCommand(Command):
	
# 	def __init__(self, connection: Connection, name):
# 		super(DelegationStartCommand, self).__init__(connection, name)
# 		self.info = 'Некоторая информация о команде'

# 	@property
# 	def socket(self):
# 		return self.connection.main_socket	

# 	def execute(self):
# 		response = spnx.delegation_start(self.socket)
# 		print(response) 