import configparser
import logging


logger_path = 'client.log'
config_path = 'client.conf'


class Logger():
	def __new__(self, name='__name__'):
		logger = logging.getLogger(name)
		logger.setLevel(logging.DEBUG)
		handler = logging.FileHandler(logger_path)
		handler.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
		handler.setFormatter(formatter)
		if not logger.hasHandlers():
			logger.addHandler(handler)
		logger.propagate = False
		return logger

	
class ConfigParser:
	def __new__(self):
		c_parser = configparser.ConfigParser()
		c_parser.read(config_path)
		return c_parser
